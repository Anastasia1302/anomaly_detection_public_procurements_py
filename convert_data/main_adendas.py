import json
import os
import re
from copy import copy, deepcopy
from datetime import datetime
import convert_data.parties as parties
from convert_data.planning import planning
from convert_data.awards import awards
from convert_data.contracts import contracts
from convert_data.tender import tender
from convert_data.budget_breakdown import budget_breakdown
from convert_data.diccionario import initialize_dict
from convert_data.convert_string import hashValue
from convert_data.convert_string import appendNameValue
import convert_data.diccionario as diccionario
import psycopg2
from convert_data.getExchangeRate import getExchangeRate

counter = -1
title_row = []
data_rows = {}
contract_data_rows = {}
contract_title_rows = {}
adenda_data_rows = {}
adenda_end_title_rows = {}
no_contract_counter = 0
end_title_row = ['id', 'ocid',
                 'contract_id',
                 'contract_startDateMonth', 'contract_endDateMonth', 'contract_period', 'award_contractStartDate_period',
                 'contract_dateSigned', 'contract_dateSigned_period', 'award_contractSignedDate_period', 'contract_amount',
                 'contract_documentTypes',
                 'contract_supplier_identifier', 'contract_supplier_contactPoint_email',
                 'contract_supplier_address_city', 'contract_supplier_memberOf', 'contract_supplier_roles',

                 'adenda_id', 'adenda_contract_dncpAmendmentType',
                 'adenda_contract_dateSignedMonth', 'adenda_contract_dateSigned_period', 'adenda_contract_period_percentage',
                 'adenda_contract_amount_increase', 'adenda_amount', 'adenda_documentTypes']


def convertToNumeric():
    global title_row
    global data_rows
    global counter
    global contract_data_rows
    global contract_title_rows
    global adenda_data_rows
    global adenda_end_title_rows
    global no_contract_counter
    global end_title_row
    contract_title_rows = {}
    contract_data_rows = {}
    outfile_dir = 'convertedData/converted_adendas_written.csv'
    dic_dir = 'convertedData/name_diccionario_adendas_written.json'
    initialize_dict(dic_dir)
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cursor = conn.cursor()
    query = "SELECT id, data from contratos_v1_1_etiqueta_tenderers " \
            "where data->'records'->0->'compiledRelease'->'tender'->>'status' not like 'active' and " \
            "data->'records'->0->'compiledRelease'->'tender'->'submissionMethod'->> 0 like 'written' order by id " \
            ";"
    cursor.execute(query)
    datos = cursor.fetchall()
    cursor.close()
    del cursor
    conn.close()
    del conn
    for registro in datos:
        print(str(registro[0]))
        data_rows = []
        counter = -1
        data_rows = {}
        row = []
        title_row = []
        data = registro[1]
        moneda_date = None
        if 'compiledRelease' in data["records"][0]:
            ocid = hashValue(data["records"][0]["compiledRelease"]['ocid'], False, True)
            title_row.append('id')
            row.append(int(registro[0]))
            title_row.append('ocid')
            row.append(ocid)
            counter = counter + 1
            data_rows[counter] = row
            if 'parties' in data["records"][0]["compiledRelease"]:
                parties.parties(data)
            adenda_title_rows = {}
            adenda_rows = {}
            adenda_counter = 0
            award_title_row = {}
            award_row = {}
            award_date = {}
            award_suppliers = {}
            if 'awards' in data["records"][0]["compiledRelease"]:
                for award in data['records'][0]['compiledRelease']['awards']:
                    aux_title_row = []
                    aux_row = []
                    [award_id, aux_title_row, aux_row, date, suppliers] = awards(award, aux_title_row, aux_row, None)
                    award_title_row[award_id] = aux_title_row
                    award_row[award_id] = aux_row
                    award_date[award_id] = date
                    award_suppliers[award_id] = suppliers

            if 'contracts' in data["records"][0]["compiledRelease"]:
                contract_title_row = {}
                contract_row = {}
                contract_ids = {}
                aux_data_rows = {}
                for o in data_rows:
                    aux_data_rows[o] = copy(data_rows[o])
                for i in range(len(data["records"][0]["compiledRelease"]['contracts'])):
                    contract = data["records"][0]["compiledRelease"]['contracts'][i]
                    [contract_id, contract_award_id, aux_title_row, aux_row,
                     contract_extendsContractID, adenda_title_row, adenda_row] = contracts(
                        contract, data["records"][0]["compiledRelease"]['parties'], [], [], moneda_date, award_date, award_suppliers, data["records"][0]["compiledRelease"]['date'])

                    contract_ids[contract_id] = contract_id
                    contract_title_row[contract_id] = aux_title_row
                    contract_row[contract_id] = aux_row

                    # print('before')
                    # print(contract_title_row)
                    # print(contract_row)
                    # print(adenda_title_row)
                    # print(adenda_row)
                    if len(adenda_title_row) > 0:
                        if contract_extendsContractID not in adenda_title_row:
                            pos = adenda_title_row.index('adenda_contract_id')
                            amp = str(adenda_row[pos]).find("ampliacion")
                            rea = str(adenda_row[pos]).find("reajuste")
                            mod = str(adenda_row[pos]).find("modificacion")
                            reno = str(adenda_row[pos]).find("renovacion")
                            pro = str(adenda_row[pos]).find("prorroga")
                            if amp > -1:
                                print('ampliacion')
                                contract_code_id = contract_id[:amp]
                                if contract_code_id[-1] == "-":
                                    contract_code_id = contract_code_id[:-1]
                                print(contract_code_id)
                                contract_extendsContractID = contract_code_id
                                appendNameValue(contract_code_id, 'contract_extendsContractID', adenda_title_row,
                                                adenda_row, None)
                                # adenda_counter = adenda_counter + 1
                                pos = adenda_title_row.index('adenda_contract_id')
                                adenda_id = deepcopy(adenda_row[pos])
                                adenda_title_rows[adenda_id] = {}
                                adenda_rows[adenda_id] = {}
                            elif rea > -1:
                                print('reajuste')
                                contract_code_id = contract_id[:rea]
                                if contract_code_id[-1] == "-":
                                    contract_code_id = contract_code_id[:-1]
                                print(contract_code_id)
                                contract_extendsContractID = contract_code_id
                                appendNameValue(contract_code_id, 'contract_extendsContractID', adenda_title_row,
                                                adenda_row, None)
                                # adenda_counter = adenda_counter + 1
                                pos = adenda_title_row.index('adenda_contract_id')
                                adenda_id = deepcopy(adenda_row[pos])
                                adenda_title_rows[adenda_id] = {}
                                adenda_rows[adenda_id] = {}
                            elif mod > -1:
                                print('modificacion')
                                contract_code_id = contract_id[:mod]
                                if contract_code_id[-1] == "-":
                                    contract_code_id = contract_code_id[:-1]
                                print(contract_code_id)
                                contract_extendsContractID = contract_code_id
                                appendNameValue(contract_code_id, 'contract_extendsContractID', adenda_title_row,
                                                adenda_row, None)
                                # adenda_counter = adenda_counter + 1
                                pos = adenda_title_row.index('adenda_contract_id')
                                adenda_id = deepcopy(adenda_row[pos])
                                adenda_title_rows[adenda_id] = {}
                                adenda_rows[adenda_id] = {}
                            elif reno > -1:
                                print('renovacion')
                                contract_code_id = contract_id[:reno]
                                if contract_code_id[-1] == "-":
                                    contract_code_id = contract_code_id[:-1]
                                print(contract_code_id)
                                contract_extendsContractID = contract_code_id
                                appendNameValue(contract_code_id, 'contract_extendsContractID', adenda_title_row,
                                                adenda_row, None)
                                # adenda_counter = adenda_counter + 1
                                pos = adenda_title_row.index('adenda_contract_id')
                                adenda_id = deepcopy(adenda_row[pos])
                                adenda_title_rows[adenda_id] = {}
                                adenda_rows[adenda_id] = {}
                            elif pro > -1:
                                print('prorroga')
                                contract_code_id = contract_id[:pro]
                                if contract_code_id[-1] == "-":
                                    contract_code_id = contract_code_id[:-1]
                                print(contract_code_id)
                                contract_extendsContractID = contract_code_id
                                appendNameValue(contract_code_id, 'contract_extendsContractID', adenda_title_row,
                                                adenda_row, None)
                                # adenda_counter = adenda_counter + 1
                                pos = adenda_title_row.index('adenda_contract_id')
                                adenda_id = deepcopy(adenda_row[pos])
                                adenda_title_rows[adenda_id] = {}
                                adenda_rows[adenda_id] = {}
                            else:
                                print('extendscode not in adenda')

                        else:
                            print('extendscode in adenda')
                            appendNameValue(contract_id, 'contract_extendsContractID', adenda_title_row, adenda_row, None)
                            contract_extendsContractID = contract_id
                            # adenda_counter = adenda_counter + 1
                            pos = adenda_title_row.index('adenda_contract_id')
                            adenda_id = deepcopy(adenda_row[pos])
                            adenda_title_rows[adenda_id] = {}
                            adenda_rows[adenda_id] = {}
                        pos = adenda_title_row.index('adenda_contract_id')
                        adenda_id = deepcopy(adenda_row[pos])
                        # if adenda_title_rows[adenda_id]:
                        adenda_title_rows[adenda_id][adenda_counter] = adenda_title_row
                        adenda_rows[adenda_id][adenda_counter] = adenda_row
                        del contract_ids[contract_id]
                        del contract_title_row[contract_id]
                        del contract_row[contract_id]

                    # print('contract_extendsContractID')
                    # print(contract_extendsContractID)
                    # print(contract_title_row)
                    # print(contract_row)
                    # print(adenda_title_row)
                    # print(adenda_row)

                # Datos generales contrato
                keys = list(contract_ids.keys())
                for l in range(1, len(keys)):
                    compare_rows(contract_row[contract_ids[keys[l - 1]]],
                                 contract_title_row[contract_ids[keys[l - 1]]],
                                 contract_row[contract_ids[keys[l]]],
                                 contract_title_row[contract_ids[keys[l]]])
                for l in reversed(range(1, len(keys))):
                    compare_rows(contract_row[contract_ids[keys[l - 1]]],
                                 contract_title_row[contract_ids[keys[l - 1]]],
                                 contract_row[contract_ids[keys[l]]],
                                 contract_title_row[contract_ids[keys[l]]])
                for l in contract_ids:
                    add_to_list({0: contract_title_row[l]},
                                {0: contract_row[l]}, copy(aux_data_rows), l)
                    for k in range(len(contract_data_rows[l])):
                        if len(contract_title_rows[l]) != len(contract_data_rows[l][k]):
                            print('datos generales')
                            print(l)
                            print(len(contract_title_rows[l]))
                            print(len(contract_data_rows[l][k]))

                # print('adendas')
                # print(adenda_title_rows)
                # print(adenda_rows)
                #
                # print('contracts')
                # print(contract_title_rows)
                # print(contract_data_rows)

                for adenda in adenda_title_rows.keys():
                    adenda_title = adenda_title_rows[adenda][0]
                    adenda_data = adenda_rows[adenda][0]
                    contract_extendsContractID_pos = adenda_title.index('contract_extendsContractID')
                    contract_extendsContractID = adenda_data[contract_extendsContractID_pos]
                    if contract_extendsContractID not in contract_title_rows:
                        for key in contract_title_rows.keys():
                            if key[:-1] == contract_extendsContractID[:-1]:
                                contract_extendsContractID = key
                                adenda_rows[adenda][0][contract_extendsContractID_pos] = key
                    if contract_extendsContractID in contract_title_rows:
                        contract_title = contract_title_rows[contract_extendsContractID]
                        contract_data = contract_data_rows[contract_extendsContractID][0]
                        if 'contract_startDate' in contract_title:
                            pos_start_date = contract_title.index('contract_startDate')
                            start_date_epoch = contract_data[pos_start_date]
                            if 'adenda_contract_dateSigned' in adenda_title:
                                pos_date = adenda_title.index('adenda_contract_dateSigned')
                                date = adenda_data[pos_date]
                                if date and start_date_epoch:
                                    period = int(date) - int(start_date_epoch)
                                    appendNameValue(int(period), 'adenda_contract_dateSigned_period', adenda_title,
                                                    adenda_data, None)
                                if 'contract_period' in contract_title:
                                    pos_total_period = contract_title.index(
                                        'contract_period')
                                    total_period = contract_data[pos_total_period]
                                    if date and start_date_epoch:
                                        partial_period = date - start_date_epoch
                                        if total_period != 0:
                                            porcentage = partial_period / total_period
                                            porcentage = porcentage * 100
                                            appendNameValue(porcentage, 'adenda_contract_period_percentage',
                                                            adenda_title, adenda_data, None)
                            if 'adenda_contract_currency' in adenda_title:
                                pos = adenda_title.index('adenda_contract_currency')
                                pos_amount = adenda_title.index('adenda_contract_amount')
                                amount = adenda_data[pos_amount]
                                moneda = adenda_data[pos]
                                if moneda == 1 and 'adenda_contract_dateSigned' not in adenda_title:
                                    start_date_epoch = datetime.fromtimestamp(start_date_epoch)
                                    exchange_rate = getExchangeRate('USD', start_date_epoch)
                                    amount = amount * exchange_rate
                                    adenda_data[pos_amount] = amount
                            if 'adenda_contract_amount' in adenda_title:
                                pos_amount = adenda_title.index('adenda_contract_amount')
                                amount = adenda_data[pos_amount]
                                appendNameValue(amount, 'adenda_amount',
                                                adenda_title, adenda_data, None)
                                if 'contract_amount' in contract_title:
                                    pos_contract_amount = contract_title.index('contract_amount')
                                    contract_amount = contract_data[pos_contract_amount]
                                    if contract_amount != 0:
                                        porcentage = amount / contract_amount
                                        porcentage = porcentage * 100
                                        appendNameValue(porcentage, 'adenda_contract_amount_increase',
                                                        adenda_title, adenda_data, None)

                # print('adenda_data_rows')
                # print(adenda_title_rows)
                # print(adenda_rows)
                # add adendas
                for m in adenda_title_rows:
                    for k in range(1, len(adenda_title_rows[m])):
                        compare_rows(adenda_rows[m][k - 1], adenda_title_rows[m][k - 1], adenda_rows[m][k],
                                     adenda_title_rows[m][k])
                    for k in reversed(range(1, len(adenda_title_rows[m]))):
                        compare_rows(adenda_rows[m][k - 1], adenda_title_rows[m][k - 1], adenda_rows[m][k],
                                     adenda_title_rows[m][k])
                for k in adenda_title_rows:
                    for l in adenda_title_rows[k]:
                        pos = adenda_title_rows[k][l].index('adenda_contract_id')
                        adenda_id = deepcopy(adenda_rows[k][l][pos])
                        pos_contract_id = adenda_title_rows[k][l].index('contract_extendsContractID')
                        contract_id = deepcopy(adenda_rows[k][l][pos_contract_id])
                        adenda_rows[k][l][pos] = hashValue(adenda_rows[k][l][pos], False, True)
                        adenda_rows[k][l][pos_contract_id] = hashValue(adenda_rows[k][l][pos_contract_id], False, True)
                        add_adenda(adenda_title_rows[k][l], adenda_rows[k][l], adenda_id, contract_id)
                # print('adenda_data_rows')
                # print(adenda_end_title_rows)
                # print(adenda_data_rows)
        else:
            with open("no_compiledRelease.txt", 'a') as err:
                err.write(str(registro[0]) + "\n")

    keys = list(adenda_data_rows.keys())
    new_contract_datarow = []
    for i in range(len(end_title_row)):
        new_contract_datarow.append(None)
    for i in range(len(list(adenda_data_rows.keys()))):
        for j in adenda_data_rows[keys[i]]:
            aux_row = copy(new_contract_datarow)
            for k in range(len(adenda_data_rows[keys[i]][j])):
                if adenda_end_title_rows[keys[i]][k] in end_title_row:
                    indice = end_title_row.index(adenda_end_title_rows[keys[i]][k])
                    if adenda_data_rows[keys[i]][j][k] is not None:
                        aux_row[indice] = copy(adenda_data_rows[keys[i]][j][k])
            adenda_data_rows[keys[i]][j] = copy(aux_row)

    data_rows = []
    title_row = convert_to_csv(end_title_row)
    for lista in adenda_data_rows.keys():
        for j in adenda_data_rows[lista].keys():
            data_rows.append(convert_to_csv(adenda_data_rows[lista][j]))

    print(len(data_rows))
    if not os.path.isfile(outfile_dir) or os.stat(outfile_dir).st_size == 0:
        with open(outfile_dir, 'a') as outfile:
            outfile.write(title_row)
            for i in data_rows:
                outfile.write("\n")
                outfile.write(i)
    else:
        print('append')
        with open(outfile_dir, 'a+') as outfile:
            for i in data_rows:
                outfile.write("\n")
                outfile.write(i)

    print('name diccionary')
    with open(dic_dir, 'w') as out:
        out.write(json.dumps(diccionario.name_diccionario))


def convert_to_csv(input_data):
    my_list = input_data
    if type(input_data) is list:
        my_list = ','.join(map(str, input_data))
    return str(my_list)


def add_to_list(sub_title_row, sub_row, new_rows, id_contrato):
    global counter
    global title_row
    global data_rows
    global contract_data_rows
    global contract_title_rows
    counter = len(data_rows)
    aux_data_rows = {}
    original_copy = None
    if type(new_rows) is dict:
        if id_contrato not in contract_data_rows.keys():
            contract_data_rows[id_contrato] = {}
            contract_title_rows[id_contrato] = copy(title_row)
        for o in new_rows:
            contract_data_rows[id_contrato][o] = copy(new_rows[o])
    else:
        for o in data_rows:
            aux_data_rows[o] = copy(data_rows[o])
    if id_contrato:
        original_copy = deepcopy(contract_data_rows[id_contrato])
        counter = len(original_copy)
    actual_counter = copy(counter)
    for l in range(0, len(sub_title_row)):
        start_counter = 0
        start_counter = start_counter + actual_counter * l
        if start_counter > 0:
            for j in range(0, actual_counter):
                if id_contrato:
                    contract_data_rows[id_contrato][counter] = copy(original_copy[j])
                    counter = counter + 1
                else:
                    data_rows[counter] = copy(aux_data_rows[j])
                    counter = counter + 1
        for m in range(len(sub_title_row[l])):
            if id_contrato:
                if start_counter > 0:
                    for n in range(start_counter, counter):
                        appendNameValue(sub_row[l][m], sub_title_row[l][m], contract_title_rows[id_contrato],
                                        contract_data_rows[id_contrato][n], contract_data_rows)
                else:
                    for n in contract_data_rows[id_contrato]:
                        appendNameValue(sub_row[l][m], sub_title_row[l][m], contract_title_rows[id_contrato],
                                        contract_data_rows[id_contrato][n], contract_data_rows)
            else:
                for n in range(start_counter, counter):
                    appendNameValue(sub_row[l][m], sub_title_row[l][m], title_row, data_rows[n], data_rows)


def add_adenda(sub_title_row, sub_row, id_adenda, id_contrato):
    try:
        global counter
        global title_row
        global data_rows
        global adenda_data_rows
        global adenda_end_title_rows
        global contract_data_rows
        global contract_title_rows
        counter = len(data_rows)
        adenda_end_title_rows[id_adenda] = deepcopy(contract_title_rows[id_contrato])
        adenda_data_rows[id_adenda] = deepcopy(contract_data_rows[id_contrato])
        for l in range(len(sub_title_row)):
            appendNameValue(sub_row[l], sub_title_row[l], adenda_end_title_rows[id_adenda],
                            adenda_data_rows[id_adenda][0], None)
    except KeyError:
        with open("contract_missing.txt", 'a') as err:
            err.write(str(id_adenda) + "\n")


def compare_rows(row1, title1, row2, title2):
    if len(title1) > len(title2):
        rellenar_campos_vacios(title1, row2, title2)
        rellenar_campos_vacios(title2, row1, title1)
    else:
        rellenar_campos_vacios(title2, row1, title1)
        rellenar_campos_vacios(title1, row2, title2)


def rellenar_campos_vacios(title1, row2, title2):
    for i in range(len(title1)):
        if title1[i] not in title2:
            if i < len(title2):
                title2.insert(i, title1[i])
                row2.insert(i, None)
            else:
                title2.append(title1[i])
                row2.append(None)


def compare_rows_dict(title1, title2, rows1, rows2):
    for i in range(len(title2)):
        if title2[i] not in title1:
            if i < len(title1):
                title1.insert(i, title2[i])
                for k in rows1:
                    rows1[k].insert(i, None)
            else:
                title1.append(title2[i])
                for k in rows1:
                    rows1[k].append(None)
    for i in range(len(title1)):
        if title1[i] not in title2:
            if i < len(title2):
                title2.insert(i, title1[i])
                for k in rows2:
                    rows2[k].insert(i, None)
            else:
                title2.append(title1[i])
                for k in rows2:
                    rows2[k].append(None)


convertToNumeric()
