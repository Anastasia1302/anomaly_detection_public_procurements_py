from convert_data.convert_string import hashValue
from convert_data.convert_string import convertArray
from convert_data.convert_string import removeDividingCharacters
from convert_data.convert_string import appendNameValue

parties_list = {}


def parties(data):
    row = []
    title_row = []
    for party in data['records'][0]['compiledRelease']['parties']:
        if 'identifier' in party:
            party_identifier = party["identifier"]
            party_identifier = hashValue(removeDividingCharacters(party_identifier['id']), party_identifier['legalName'], False)
            appendNameValue(party_identifier, 'identifier', title_row, row, None)
        if 'contactPoint' in party:
            # if 'name' in party["contactPoint"]:
            #     party_contactpoint_name = party["contactPoint"]['name']
            #     if party_contactpoint_name is not None:
            #         party_contactpoint_name = hashValue(party_contactpoint_name, False, True)
            #         appendNameValue(party_contactpoint_name, 'contactPoint_name', title_row, row, None)
            if 'email' in party["contactPoint"]:
                party_contactpoint_email = party["contactPoint"]['email']
                if party_contactpoint_email is not None:
                    party_contactpoint_email = hashValue(party_contactpoint_email, False, True)
                    appendNameValue(party_contactpoint_email, 'contactPoint_email', title_row, row, None)
        if 'address' in party:
            # if 'streetAddress' in party['address']:
            #     party_contactpoint_streetaddress = party["address"]['streetAddress']
            #     party_contactpoint_streetaddress = hashValue(party_contactpoint_streetaddress, False, True)
            #     appendNameValue(party_contactpoint_streetaddress, 'address_streetAddress', title_row, row, None)
            [party_address_region, party_address_locality, party_address_countryname] = limpiar_datos(party["address"]['region'], party["address"]['locality'], party["address"]['countryName'])
            if party_address_region is not None:
                address = party_address_region
                address = hashValue(address, False, True)
                appendNameValue(address, 'address_city', title_row, row, None)
        if 'memberOf' in party:
            party_member_of = party["memberOf"][0]
            party_member_of = hashValue(party_member_of['id'], party_member_of['name'], True)
            appendNameValue(int(party_member_of), 'memberOf', title_row, row, None)
        if 'roles' in party:
            party_roles = convertArray(party['roles'], 'roles')
            appendNameValue(int(party_roles), 'roles', title_row, row, None)
        parties_list[party['id']] = {"title_row": title_row, "row": row}
        row = []
        title_row = []


def limpiar_datos(ciudad, departamento, pais):
    if ciudad is not None:
        ciudad = ciudad.upper().replace('-', '').replace('(MUNICIPIO)', '').replace("( MUNICIPIO )", '').replace('.', ''). replace('(DISTRITO)', '')
    if departamento is not None:
        departamento = departamento.upper().strip().replace('-', '')
    if pais is not None:
        pais = pais.upper().strip().replace('-', '')
    if ciudad is not None:
        if "ASUN" in ciudad:
            if departamento == 'CENTRAL' or pais == 'PARAGUAY':
                ciudad = "ASUNCIÓN"
                departamento = 'CENTRAL'
                pais = 'PARAGUAY'

    if departamento is not None:
        if departamento == "ASUNCIÓN":
            ciudad = "ASUNCIÓN"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "EMBY" in ciudad:
            ciudad = "ÑEMBY"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif 'FILA' in ciudad or 'FERN' in ciudad:
            ciudad = 'FILADELFIA'
            departamento = 'BOQUERÓN'
            pais = 'PARAGUAY'
        elif "ACUNCION" in ciudad:
            ciudad = "ASUNCIÓN"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "AREG" in ciudad:
            ciudad = "AREGUÁ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "AUSNCION" in ciudad:
            ciudad = "ASUNCIÓN"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "BELLA VISTA" in ciudad:
            ciudad = "BELLA VISTA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "AYOLAS" in ciudad:
            ciudad = "AYOLAS"
            departamento = 'MISIONES'
            pais = 'PARAGUAY'
        elif "BENJAMIN ACEVAL" in ciudad:
            ciudad = "BENJAMIN ACEVAL"
            departamento = 'PRESIDENTE HAYES'
            pais = 'PARAGUAY'
        elif "CAAC" in ciudad:
            ciudad = "CAACUPÉ"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "CAAG" in ciudad:
            ciudad = "CAAGUAZÚ"
            departamento = 'CAAGUAZÚ'
            pais = 'PARAGUAY'
        elif "CAAZ" in ciudad:
            ciudad = "CAAZAPÁ"
            departamento = 'CAAZAPÁ'
            pais = 'PARAGUAY'
        elif "CAPIA" in ciudad:
            ciudad = "CAPIATÁ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "CAPII" in ciudad:
            ciudad = "CAPIIBARY"
            departamento = 'SAN PEDRO'
            pais = 'PARAGUAY'
        elif "CAPITAN MIRANDA" in ciudad:
            ciudad = "CAPITÁN MIRANDA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "CARAP" in ciudad:
            ciudad = "CARAPEGUÁ"
            departamento = 'PARAGUARÍ'
            pais = 'PARAGUAY'
        elif "CARLOS" in ciudad:
            ciudad = "CARLOS ANTONIO LÓPEZ"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "CDE" in ciudad or "CIUDAD DEL ESTE" in ciudad or "CUIDAD DEL ESTE" in ciudad or "DEL ESTE" in ciudad:
            ciudad = "CIUDAD DEL ESTE"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "CHOR" in ciudad:
            ciudad = "CHORÉ"
            departamento = 'SAN PEDRO'
            pais = 'PARAGUAY'
        elif "BUENOS AIRES" in ciudad:
            ciudad = "BUENOS AIRES"
            departamento = 'BUENOS AIRES'
            pais = 'ARGENTINA'
        elif "OVIEDO" in ciudad and "RAUL" not in ciudad:
            ciudad = "CORONEL OVIEDO"
            departamento = 'CAAGUAZÚ'
            pais = 'PARAGUAY'
        elif "COLONIA IRU" in ciudad:
            ciudad = "COLONIA IRUÑA"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "CONC" in ciudad:
            ciudad = "CONCEPCIÓN"
            departamento = 'CONCEPCIÓN'
            pais = 'PARAGUAY'
        elif "CNEL MARTINEZ" in ciudad:
            ciudad = "CORONEL MARTINEZ"
            departamento = 'GUAIRÁ'
            pais = 'PARAGUAY'
        elif "CORRIENTE" in ciudad:
            ciudad = "CORRIENTES"
            departamento = 'ARGENTINA'
            pais = 'ARGENTINA'
        elif "CURITIBA" in ciudad:
            ciudad = "CURITIBA"
            departamento = 'PARANÁ'
            pais = 'BRASIL'
        elif "MBARACAY" in ciudad:
            ciudad = "MBARACAYÚ"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "DOMINGO M" in ciudad:
            ciudad = "DOMINGO MARTINEZ DE IRALA"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "AYALA" in ciudad:
            ciudad = "EUSEBIO AYALA"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "EDEL" in ciudad:
            ciudad = "EDELIRA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "ENCAR" in ciudad:
            ciudad = "ENCARNACIÓN"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "DE LA MORA" in ciudad or "FERNANDO DE MORA" in ciudad:
            ciudad = "FERNANDO DE LA MORA"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "FRANCISCO CABALLERO" in ciudad:
            ciudad = "FRANCISCO CABALLERO ÁLVAREZ"
            departamento = 'CANINDEYÚ'
            pais = 'PARAGUAY'
        elif "GRAL AR" in ciudad:
            ciudad = "GENERAL ARTIGAS"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "GRAL DELGADO" in ciudad:
            ciudad = "GENERAL DELGADO"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "GUAYA" in ciudad or "GUAJ" in ciudad:
            ciudad = "GUAYAIBÍ"
            departamento = 'SAN PEDRO'
            pais = 'PARAGUAY'
        elif "HERNA" in ciudad:
            ciudad = "HERNANDARIAS"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "HOHENAU" in ciudad:
            ciudad = "HOHENAU"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "ITA" in ciudad:
            ciudad = "ITÁ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "ITACDEL ROSARIO" in ciudad:
            ciudad = "ITACURUBI DEL ROSARIO"
            departamento = 'SAN PEDRO'
            pais = 'PARAGUAY'
        elif "ITACURUBI DE LA CORDILLERA" in ciudad:
            ciudad = "ITACURUBÍ DE LA CORDILLERA"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "ITAU" in ciudad or "ITAG" in ciudad:
            ciudad = "ITAUGUÁ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "ITAPA" in ciudad or "ITAPÁ" in ciudad:
            ciudad = "ITAPÚA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "AUGUSTO SALDIVAR" in ciudad or "JA SALDIVAR" in ciudad or "JASALDIVAR" in ciudad:
            ciudad = "J AUGUSTO SALDÍVAR"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "J E ESTIGARRIBIA" in ciudad or "JE ESTIGARRIBIA" in ciudad or "JEULOGIO ESTIGARRIBIA" in ciudad:
            ciudad = "J EULOGIO ESTIGARRIBIA"
            departamento = 'CAAGUAZÚ'
            pais = 'PARAGUAY'
        elif "OCAMPOS" in ciudad:
            ciudad = "JOSÉ DOMINGO OCAMPOS"
            departamento = 'CAAGUAZÚ'
            pais = 'PARAGUAY'
        elif "LEARY" in ciudad:
            ciudad = "JUAN E O LEARY"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "MALLORQUIN" in ciudad:
            ciudad = "JUAN LEÓN MALLORQUÍN"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "OTAÑO" in ciudad:
            ciudad = "JULIO D OTAÑO"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "LA PALOMA" in ciudad:
            ciudad = "LA PALOMA"
            departamento = 'CANINDEYÚ'
            pais = 'PARAGUAY'
        elif "LAM" in ciudad:
            ciudad = "LAMBARÉ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "LUMPIO" in ciudad:
            ciudad = "LIMPIO"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "MARIA AUXILIADORA" in ciudad:
            ciudad = "MARÍA AUXILIADORA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "MARIANO" in ciudad or "MRALONSO" in ciudad:
            ciudad = "MARIANO ROQUE ALONSO"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "MAYOR JDMARTINEZ" in ciudad:
            ciudad = "MAYOR MARTINEZ"
            departamento = 'ÑEEMBUCÚ'
            pais = 'PARAGUAY'
        elif "MCAL" in ciudad or "ESTIGARRIBIA" in ciudad:
            ciudad = "MARISCAL ESTIGARRIBIA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "MINGA G" in ciudad:
            ciudad = "MINGA GUAZÚ"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "MONTEVIDEO" in ciudad:
            ciudad = "MONTEVIDEO"
            # departamento = 'MONTEVIDEO'
            # pais = 'URUGUAY'
        elif "MUMBAI" in ciudad:
            ciudad = "MUMBAI"
        elif "NUEVA ESPERANZA" in ciudad:
            ciudad = "NUEVA ESPERANZA"
            departamento = 'CANINDEYÚ'
            pais = 'PARAGUAY'
        elif "NULL" in ciudad:
            ciudad = "DESCONOCIDO"
            # departamento = 'ITAPÚA'
            # pais = 'PARAGUAY'
        elif "PARAGAUYRI" in ciudad:
            ciudad = "PARAGUARÍ"
            departamento = 'PARAGUARÍ'
            pais = 'PARAGUAY'
        elif "JUAN CABALLERO" in ciudad or "PEDRO JUANCABALLERO" in ciudad:
            ciudad = "PEDRO JUAN CABALLERO"
            departamento = 'AMAMBAY'
            pais = 'PARAGUAY'
        elif "PDTEFRANCO" in ciudad or "PDTE FRANCO" in ciudad or "PDTFRANCO" in ciudad or "PDT FRANCO" in ciudad:
            ciudad = "PRESIDENTE FRANCO"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "PENDIENTE ACTUALIZACION DATOS" in ciudad:
            ciudad = "DESCONOCIDO"
        elif "PILAE" in ciudad:
            ciudad = "PILAR"
            departamento = 'ÑEEMBUCÚ'
            pais = 'PARAGUAY'
        elif "RAUL A OVIEDO" in ciudad:
            ciudad = "RAUL ARSENIO OVIEDO"
            departamento = 'CAAGUAZÚ'
            pais = 'PARAGUAY'
        elif "SALTO" in ciudad:
            ciudad = "SALTO DEL GUAIRÁ"
            departamento = 'CANINDEYÚ'
            pais = 'PARAGUAY'
        elif "SAN COSME Y DAMIAN" in ciudad:
            ciudad = "SAN COSME Y DAMIÁN"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "SAN IGNACIO" in ciudad:
            ciudad = "SAN IGNACIO"
            departamento = 'MISIONES'
            pais = 'PARAGUAY'
        elif "NEPOMUCENO" in ciudad:
            ciudad = "SAN JUAN NEPOMUCENO"
            departamento = 'CAAZAPÁ'
            pais = 'PARAGUAY'
        elif "BAUTISTA" in ciudad:
            ciudad = "SAN JUAN BAUTISTA"
            departamento = 'MISIONES'
            pais = 'PARAGUAY'
        elif "LAZARO" in ciudad:
            ciudad = "SAN LÁZARO"
            departamento = 'CONCEPCIÓN'
            pais = 'PARAGUAY'
        elif "LORENZO" in ciudad or "LORENSO" in ciudad:
            ciudad = "SAN LORENZO"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "SAN MIGUEL" in ciudad:
            ciudad = "SAN MIGUEL"
            departamento = 'MISIONES'
            pais = 'PARAGUAY'
        elif "SAN PEDRO" in ciudad and departamento == "SAN PEDRO":
            ciudad = "SAN PEDRO DE YCUAMANDYYU"
            departamento = 'SAN PEDRO'
            pais = 'PARAGUAY'
        elif "SAN PEDRO DEL PARANÁ" in ciudad and departamento == "ITAPÚA":
            ciudad = "ENCARNACIÓN"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "SAN RAFAEL DEL PARANA" in ciudad:
            ciudad = "SAN RAFAEL DEL PARANÁ"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "SAN ROQUE GONZALEZ" in ciudad:
            ciudad = "SAN ROQUE GONZÁLEZ DE SANTA CRUZ"
            departamento = 'PARAGUARÍ'
            pais = 'PARAGUAY'
        elif "SANTA ROSA DEL MODAY" in ciudad:
            ciudad = "SANTA ROSA DEL MONDAY"
            departamento = 'ALTO PARANÁ'
            pais = 'PARAGUAY'
        elif "SANTIAGO" in ciudad and 'MISIONES' in departamento:
            ciudad = "SANTIAGO MISIONES"
            pais = 'PARAGUAY'
        elif "TOBAT" in ciudad:
            ciudad = "TOBATÍ"
            departamento = 'CORDILLERA'
            pais = 'PARAGUAY'
        elif "TOMAS ROMERO PEREIRA" in ciudad:
            ciudad = "TOMÁS ROMERO PEREIRA"
            departamento = 'ITAPÚA'
            pais = 'PARAGUAY'
        elif "MANUEL IRALA FERNANDEZ" in ciudad:
            ciudad = "TTE MANUEL IRALA FERNANDEZ"
            departamento = 'PRESIDENTE HAYES'
            pais = 'PARAGUAY'
        elif "VALLE" in ciudad:
            ciudad = "VALLEMÍ"
            departamento = 'CONCEPCIÓN'
            pais = 'PARAGUAY'
        elif "VILLA ELISA" in ciudad or "VILLAELISA" in ciudad or "VILLA ELIZA" in ciudad or "VILLAELIZA" in ciudad:
            ciudad = "VILLA ELISA"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "VILLARRICA" in ciudad or "VILARRICA" in ciudad or "VILLARICA" in ciudad:
            ciudad = "VILLARRICA"
            departamento = 'GUAIRÁ'
            pais = 'PARAGUAY'
        elif "YAGUARON" in ciudad:
            ciudad = "YAGUARÓN"
            departamento = 'PARAGUARÍ'
            pais = 'PARAGUAY'
        elif "YBY" in ciudad:
            ciudad = "YBY YAÚ"
            departamento = 'CONCEPCIÓN'
            pais = 'PARAGUAY'
        elif "YPACARA" in ciudad:
            ciudad = "YPACARAÍ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "YPANE" in ciudad:
            ciudad = "YPANÉ"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
        elif "YUTY" in ciudad:
            ciudad = "YUTY"
            departamento = 'CAAZAPÁ'
            pais = 'PARAGUAY'

    return [ciudad, departamento, pais]
