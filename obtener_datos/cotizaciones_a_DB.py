from pyexcel_xlsx import get_data
import psycopg2

# https://www.bcp.gov.py/cotizacion-minorista-del-tipo-de-cambio-nominal-i368

indir = "cotizaciones/"

meses = {
    1: '01',
    2: '02',
    3: '03',
    4: '04',
    5: '05',
    6: '06',
    7: '07',
    8: '08',
    9: '09',
    10: '10',
    11: '11',
    12: '12',
}
dias = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31,
}


def datos_bcp():
    # [28, 5827.142857142857, 5867.142857142857, 1410.7142857142858, 1444.2857142857142, 140.33333333333334, 152.66666666666666, 6577.142857142857, 6906.428571428572, 44.333333333333336, 55.666666666666664]
    compra_ant = 5998.571428571428
    venta_ant = 6064.285714285715
    year = "2019"
    # # for j in range(, 12):
    # # for j in range(12):
    j = 7
    mes = meses.get(j)
    days = dias.get(j)
    print(mes)
    print(days)
    file = indir + year + '/Cotizaciones_' + mes + '_' + year + '.xlsx'
    data = get_data(file)
    print(data)

    key1 = 'Cotizaciones Diarias'
    key2 = 'Cotización Diaria'
    key3 = 'JULIO 11'
    key = key1
    print(data.keys())
    start = 11
    for i in range(days):
        print(data[key][start+i])
        # if i == 0 or i == 1:
        #     # if i == 30:
        #     compra = compra_ant
        #     venta = venta_ant
        #     fecha = year + '-' + mes + '-' + str(i+1)
        # else:

        compra = data[key][start+i][1]
        venta = data[key][start+i][2]
        if compra == '-':
            compra = compra_ant
        if venta == '-':
            venta = venta_ant
        fecha = year + '-' + mes + '-' + str(data[key][start + i][0])

        compra_ant = compra
        venta_ant = venta
        try:
            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "INSERT INTO cambios_dolar_gs_bnf (fecha, compra, venta) " \
                    "VALUES (%s, %s, %s);"
            d = (fecha, compra, venta)
            cursor.execute(query, d)
            conn.commit()
            cursor.close()
            del cursor
            conn.close()
            del conn
        except psycopg2.IntegrityError as e:
            print(e)


datos_bcp()
