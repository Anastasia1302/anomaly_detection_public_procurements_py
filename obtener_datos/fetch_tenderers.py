import optparse
import requests
import requests_cache
import json
import csv
from ratelimit import *
import psycopg2
import time

# Search, fetch records, fetch releases

year = 2016
error = 0
total = 0


@rate_limited(15)
def fetchData(id_list, folder, page=0):
    print(id_list)
    global year
    global error
    global total
    record_id = id_list[page]

    total = total + 1
    #     record_id) + " > https://www.contrataciones.gov.py:443/datos/api/v2/doc/convocatorias/" + str(record_id))
    # r = requests.get('https://www.contrataciones.gov.py:443/datos/api/v2/doc/convocatorias/' + str(record_id))
    print("Fetching record " + str(page) + '/' + str(len(id_list)) + ' ID: ' + str(
        record_id) + " > https://www.contrataciones.gov.py:443/datos/api/v2/doc/adjudicaciones/" + str(
        record_id) + "/oferentes")
    r = requests.get('https://www.contrataciones.gov.py:443/datos/api/v2/doc/adjudicaciones/' + str(
        record_id) + "/oferentes")

    rr = r.status_code
    print(r)
    while rr == 429:
        print('sleep 10 seconds then retry')
        time.sleep(10)
        print("Fetching record " + str(page) + '/' + str(len(id_list)) + ' ID: ' + str(
            record_id) + " > https://www.contrataciones.gov.py:443/datos/api/v2/doc/adjudicaciones/" + str(
            record_id) + "/oferentes")
        r = requests.get(
            'https://www.contrataciones.gov.py:443/datos/api/v2/doc/adjudicaciones/' + str(record_id) + "/oferentes")
        print(r)
        rr = r.status_code

    try:
        conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
        cursor = conn.cursor()
        query = "INSERT INTO tenderes (id, secondary_id, data) VALUES (%s, %s, %s);"
        print(record_id)
        print(record_id[:6])
        data = (record_id[0:6], record_id, json.dumps(r.json(), indent=2, ensure_ascii=False))
        print(data)
        cursor.execute(query, data)
        conn.commit()
        cursor.close()
        del cursor
        conn.close()
        del conn

    except Exception as e:
        error = error + 1
        cursor.close()
        del cursor
        conn.close()
        del conn
        with open("errors-tenderes-2018.txt", 'a') as err:
            print('Error: ' + str(e))
            if str(e)[:13] != "duplicate key" and rr != 500 and rr != 404:
                err.write(str(record_id) + "\n")

    page = int(page) + 1
    with open("page.n", 'w') as n:
        n.write(str(page))

    if page < len(id_list):
        fetchData(id_list, folder, page)
    else:
        with open("page.n", 'w') as n:
            n.write("0")


def fetchList():
    print("Fetching listing")
    id_list = []

    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cursor = conn.cursor()
    query = "SELECT jsonb_array_elements(data->'records')->'compiledRelease'->'award'->'id' as award_id from contratos;"
    cursor.execute(query)
    rows = cursor.fetchall()
    cursor.close()
    del cursor
    conn.close()
    del conn
    for line in rows:
        if len(line) == 1:
            if line[0] is not None:
                id_list.append(line[0])
        else:
            print(line)
            with open("more_than_1_dimension.txt", 'a') as err:
                err.write(str(line[0]) + "\n")

    return id_list


def fetch_err_list():
    file_name = "errors-tenderers.txt"
    print("Fetching " + str(file_name) + " listing")
    id_list = []
    f = open(file_name, 'r')
    for line in f:
        print(str(line).rstrip())
        id_list.append(line.rstrip())
    return id_list[0:]


def main():
    global error
    global total
    global year
    requests_cache.install_cache('test_cache', backend='sqlite', expire_after=300)

    usage = 'Usage: %prog [ --all --cont ]'
    parser = optparse.OptionParser(usage=usage)

    parser.add_option('-R', '--resume', action='store_true', default=False,
                      help='Continue from the last page (in page.n), for when download broken')

    parser.add_option('-e', '--error', action='store_true', default=False,
                      help='Retry items saved in error.txt file')

    (options, args) = parser.parse_args()

    if options.error:
        id_list = fetch_err_list()
    else:
        id_list = fetchList()

    if options.resume:
        with open("page.n", 'r') as n:
            page = n.read()
    else:
        page = 0
    print(id_list)
    fetchData(id_list, 'all', int(page))
    print(str(error) + "/" + str(total))


if __name__ == '__main__':
    main()
