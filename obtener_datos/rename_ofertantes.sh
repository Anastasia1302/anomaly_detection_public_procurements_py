#!/usr/bin/env bash

for g in ~/PycharmProjects/change_version_1_1_ocds/ofertantes/ofertantes/datos/*
do
    new=$(basename "$g" .json)
	new=${new:0:6}
    sudo mv ${g} ~/PycharmProjects/change_version_1_1_ocds/ofertantes/ofertantes/rename/${new}.json
done