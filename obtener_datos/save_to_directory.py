import json
import psycopg2

def save_to_directory():
    from psycopg2.extras import register_json
    register_json(oid=3802, array_oid=3807)
    query = """
        Select id, data from contratos_con_etiqueta;
        """
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cur = conn.cursor()
    cur.execute(query)
    contratos = cur.fetchall()
    cur.close()
    del cur
    conn.close()
    del conn
    for row in contratos:
        print(row[0])
        try:
            data = row[1]
            with open('/home/mekehler/Documentos/contratos_change_version/' +
                      str(row[0]) + '.json', 'w') as outfile:
                json.dump(data, outfile)
        except Exception as e:
            print('Error: ' + str(e))
            if str(e)[:13] != "duplicate key":
                with open("save-to-directory.txt", 'a') as err:
                    err.write(str(row[0]) + "\n")


save_to_directory()