from train_models.validaciones_protestas_denuncias import validaciones_protestas_denuncias
from train_models.validate_model_with_new_data import validate_with_new_data
import pandas as pd
import re
import os
name = 'adendas_written'
version = '_with_anomalies_validation_no_hacienda_amount'
direccion = 'models/' + name + '_no_amount_increase' + version + '/'

textfile_training = "../convert_data/convertedData/converted_" + name + "_training_set" + version + ".csv"
textfile_validation = "../convert_data/convertedData/converted_" + name + "_validation_set" + version + ".csv"

set_direccion = 'validaciones/' + name + '_no_amount_increase' + version + '.csv'

resultados = pd.DataFrame()
result_estimators = []
result_samples = []
result_features = []
result_rep = []
result_anomalias_training = []
result_anomalias_training_percent = []
result_anomalias_muy_anomalos_training = []
result_anomalias_muy_normales_training = []
total_training = []
result_anomalias_protestas = []
result_anomalias_protestas_percent = []
result_anomalias_muy_anomalos_protestas = []
result_anomalias_muy_normales_protestas = []
total_protestas = []
result_anomalias_denuncias = []
result_anomalias_denuncias_percent = []
result_anomalias_muy_anomalos_denuncias = []
result_anomalias_muy_normales_denuncias = []
total_denuncias = []
result_anomalias_validation = []
result_anomalias_validation_percent = []
result_anomalias_muy_anomalos_validation = []
result_anomalias_muy_normales_validation = []
total_validation = []
result_anomalias_validation_protestas = []
result_anomalias_validation_percent_protestas = []
result_anomalias_muy_anomalos_validation_protestas = []
result_anomalias_muy_normales_validation_protestas = []
total_validation_protestas = []
result_anomalias_validation_denuncias = []
result_anomalias_validation_percent_denuncias = []
result_anomalias_muy_anomalos_validation_denuncias = []
result_anomalias_muy_normales_validation_denuncias = []
total_validation_denuncias = []

for filename in sorted(os.listdir(direccion)):
    estimator = re.search('estimators_(.*)_samples', filename)
    samples = re.search('samples_(.*)_features', filename)
    # if int(samples.group(1)) <= 100 and int(estimator.group(1)) == 20:
    if filename:
        # if estimator.group(1) == '100':
        print(filename)

        predictions_training = validate_with_new_data(direccion + filename, textfile_training, ',')
        predictions_training = validaciones_protestas_denuncias(predictions_training)

        total_training.append(predictions_training["total"][0])
        result_anomalias_training.append(predictions_training["anomalias"][0])
        result_anomalias_training_percent.append((predictions_training["anomalias"][0]/predictions_training["total"][0])*100)
        result_anomalias_muy_anomalos_training.append(predictions_training["muy_anormales"][0])
        result_anomalias_muy_normales_training.append(predictions_training["muy_normales"][0])

        total_protestas.append(predictions_training["total_protestas"][0])
        result_anomalias_protestas.append(predictions_training["anomalias_protestas"][0])
        result_anomalias_protestas_percent.append(
            (predictions_training["anomalias_protestas"][0] / predictions_training["total_protestas"][0])*100)
        result_anomalias_muy_anomalos_protestas.append(predictions_training["muy_anormales_protestas"][0])
        result_anomalias_muy_normales_protestas.append(predictions_training["muy_normales_protestas"][0])

        total_denuncias.append(predictions_training["total_denuncias"][0])
        result_anomalias_denuncias.append(predictions_training["anomalias_denuncias"][0])
        result_anomalias_denuncias_percent.append(
            (predictions_training["anomalias_denuncias"][0] / predictions_training["total_denuncias"][0])*100)
        result_anomalias_muy_anomalos_denuncias.append(predictions_training["muy_anormales_denuncias"][0])
        result_anomalias_muy_normales_denuncias.append(predictions_training["muy_normales_denuncias"][0])

        predictions_validation = validate_with_new_data(direccion + filename, textfile_validation, ',')
        predictions_validation = validaciones_protestas_denuncias(predictions_validation)

        total_validation.append(predictions_validation["total"][0])
        result_anomalias_validation.append(predictions_validation["anomalias"][0])
        result_anomalias_validation_percent.append((predictions_validation["anomalias"][0] / predictions_validation["total"][0]) * 100)
        result_anomalias_muy_anomalos_validation.append(predictions_validation["muy_anormales"][0])
        result_anomalias_muy_normales_validation.append(predictions_validation["muy_normales"][0])

        total_validation_protestas.append(predictions_validation["total_protestas"][0])
        result_anomalias_validation_protestas.append(predictions_validation["anomalias_protestas"][0])
        result_anomalias_validation_percent_protestas.append(
            (predictions_validation["anomalias_protestas"][0] / predictions_validation["total_protestas"][0]) * 100)
        result_anomalias_muy_anomalos_validation_protestas.append(predictions_validation["muy_anormales_protestas"][0])
        result_anomalias_muy_normales_validation_protestas.append(predictions_validation["muy_normales_protestas"][0])

        total_validation_denuncias.append(predictions_validation["total_denuncias"][0])
        result_anomalias_validation_denuncias.append(predictions_validation["anomalias_denuncias"][0])
        result_anomalias_validation_percent_denuncias.append(
            (predictions_validation["anomalias_denuncias"][0] / predictions_validation["total_denuncias"][0]) * 100)
        result_anomalias_muy_anomalos_validation_denuncias.append(predictions_validation["muy_anormales_denuncias"][0])
        result_anomalias_muy_normales_validation_denuncias.append(predictions_validation["muy_normales_denuncias"][0])

        estimator = re.search('estimators_(.*)_samples', filename)
        result_estimators.append(int(estimator.group(1)))
        samples = re.search('samples_(.*)_features', filename)
        result_samples.append(int(samples.group(1)))
        # features = re.search('_features_(.*).joblib', filename)
        # print(features)
        # subfeatures = re.search('_features_(.*)', filename)
        subfeatures = re.search('_features_(.*)', filename)
        rep = re.search('_(.*).joblib', subfeatures.group(1))
        # print(rep.group(1))
        # print(subfeatures.group(1))
        features = re.search('(.*)_1.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_2.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_3.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_4.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_5.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_6.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_7.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_8.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_9.joblib', subfeatures.group(1))
        if not features:
            features = re.search('(.*)_10.joblib', subfeatures.group(1))
        # print(features.group(1))
        result_rep.append(int(rep.group(1)))
        result_features.append(int(features.group(1)))

resultados["estimators"] = result_estimators
resultados["samples"] = result_samples
resultados["features"] = result_features
resultados["rep"] = result_rep
resultados["anomalias_training"] = result_anomalias_training
resultados["anomalias_training_percent"] = result_anomalias_training_percent
resultados["muy_anormales_training"] = result_anomalias_muy_anomalos_training
resultados["muy_normales_training"] = result_anomalias_muy_normales_training
resultados["total_training"] = total_training
resultados["anomalias_protestas"] = result_anomalias_protestas
resultados["anomalias_protestas_percent"] = result_anomalias_protestas_percent
resultados["muy_anormales_protestas"] = result_anomalias_muy_anomalos_protestas
resultados["muy_normales_protestas"] = result_anomalias_muy_normales_protestas
resultados["total_protestas"] = total_protestas
resultados["anomalias_denuncias"] = result_anomalias_denuncias
resultados["anomalias_denuncias_percent"] = result_anomalias_denuncias_percent
resultados["muy_anormales_denuncias"] = result_anomalias_muy_anomalos_denuncias
resultados["muy_normales_denuncias"] = result_anomalias_muy_normales_denuncias
resultados["total_denuncias"] = total_denuncias

resultados["anomalias_validation"] = result_anomalias_validation
resultados["anomalias_validation_percent"] = result_anomalias_validation_percent
resultados["muy_anormales_validation"] = result_anomalias_muy_anomalos_validation
resultados["muy_normales_validation"] = result_anomalias_muy_normales_validation
resultados["total_validation"] = total_validation
resultados["anomalias_validation_protestas"] = result_anomalias_validation_protestas
resultados["anomalias_validation_percent_protestas"] = result_anomalias_validation_percent_protestas
resultados["muy_anormales_validation_protestas"] = result_anomalias_muy_anomalos_validation_protestas
resultados["muy_normales_validation_protestas"] = result_anomalias_muy_normales_validation_protestas
resultados["total_validation_protestas"] = total_validation_protestas
resultados["anomalias_validation_denuncias"] = result_anomalias_validation_denuncias
resultados["anomalias_validation_percent_denuncias"] = result_anomalias_validation_percent_denuncias
resultados["muy_anormales_validation_denuncias"] = result_anomalias_muy_anomalos_validation_denuncias
resultados["muy_normales_validation_denuncias"] = result_anomalias_muy_normales_validation_denuncias
resultados["total_validation_denuncias"] = total_validation_denuncias

resultados.sort_values(by=['estimators', 'samples', 'features'], inplace=True)

print(resultados)

resultados.to_csv(set_direccion, header=['estimators', 'samples', 'features',
                                         'rep',
                                         'anomalias_training', 'anomalias_training_percent', 'muy_anormales_training',
                                         'muy_normales_training',
                                         'total_training',
                                         'anomalias_protestas', 'anomalias_protestas_percent',
                                         'muy_anormales_protestas', 'muy_normales_protestas',
                                         'total_protestas',
                                         'anomalias_denuncias', 'anomalias_denuncias_percent',
                                         'muy_anormales_denuncias', 'muy_normales_denuncias',
                                         'total_denuncias',

                                         'anomalias_validation', 'anomalias_validation_percent',
                                         'muy_anormales_validation', 'muy_normales_validation',
                                         'total_validation',
                                         'anomalias_validation_protestas', 'anomalias_validation_percent_protestas',
                                         'muy_anormales_validation_protestas', 'muy_normales_validation_protestas',
                                         'total_validation_protestas',
                                         'anomalias_validation_denuncias', 'anomalias_validation_percent_denuncias',
                                         'muy_anormales_validation_denuncias', 'muy_normales_validation_denuncias',
                                         'total_validation_denuncias'
                                         ], index=None, sep=',', mode='w')
