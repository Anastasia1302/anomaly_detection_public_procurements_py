import pandas as pd
from joblib import dump, load

name = 'tender_written'
version = '_with_anomalies_validation_no_hacienda'
direccion = 'models/' + name + '_no_numberOfTenderers' + version + '/'

model_name = name + '_no_numberOfTenderers' + version + '_model_estimators_50_samples_20_features_23_2.joblib'

# textfile_training = "../convert_data/convertedData/converted_" + name + "_training_set" + version + ".csv"
# textfile_validation = "../convert_data/convertedData/converted_" + name + "_validation_set" + version + ".csv"
# textfile_2019 = "../convert_data/convertedData/converted_" + name + "_2019.csv"
new_file = "../convert_data/convertedData/converted_tender_written_371616.csv"

set_direccion = 'iforest_results/' + name + '_training_no_contract_supplier' + version + \
                '_model_estimators_50_samples_20_features_24_2.csv'

input_data = pd.read_csv(new_file, sep=';')
print(input_data)
# data_training = pd.read_csv(textfile_training, sep=';')
# data_validation = pd.read_csv(textfile_validation, sep=';')
# data_2019 = pd.read_csv(textfile_2019, sep=';')
# print(len(data_training))
# print(len(data_validation))
# print(len(data_2019))

# frames = [data_training, data_validation, data_2019]
# input_data = pd.concat(frames)

# input_data = data_training.append(data_validation, ignore_index=True)
# input_data = input_data.append(data_2019, ignore_index=True)
# print(input_data)

# drop id and ocid, tender_id (2, 6), award_id(28), contract_id(2, 34) y adenda_id (18)
# if 'tender' in textfile_training:
data = input_data.drop(['id', 'ocid', 'tender_id'], axis=1)
print(data)
# elif 'adenda' in textfile_training:
#     data = input_data.drop(input_data.columns[[0, 1, 2, 18]], axis=1)
#     data = data.drop(['contract_supplier_roles', 'contract_status'], axis=1)
# else:
#     data = input_data.drop(input_data.columns[[0, 1, 6, 28, 30, 34]], axis=1)

data.replace(to_replace='None', value=0, inplace=True)
data.replace(to_replace='NaN', value=0, inplace=True)
data.fillna(0)
print(data)
clf = load(direccion + model_name)
# print(clf)
prediction = clf.predict(data)
print(prediction)

# 1 is inlier, -1 outlier
scores = clf.decision_function(data)
print(scores)

# predictions = pd.DataFrame()
# predictions["id"] = input_data["id"]
#
# header = []
# if 'tender' in textfile_training or 'contract' in textfile_training or 'contratos' in textfile_training:
#     predictions["tender_id"] = input_data["tender_id"]
#     header = ['OCDS ID', 'Tender Id', 'Predicted Class', 'Scores']
# if 'contract' in textfile_training or 'contratos' in textfile_training:
#     predictions["contract_id"] = input_data["contract_id"]
#     header = ['OCDS ID', 'Tender Id', 'Contract Id', 'Predicted Class', 'Scores']
# if 'adenda' in textfile_training:
#     predictions["contract_id"] = input_data["contract_id"]
#     predictions["adenda_id"] = input_data["adenda_id"]
#     header = ['OCDS ID', 'Tender Id', 'Contract Id', 'Adenda Id', 'Predicted Class', 'Scores']
#
# predictions["predicted_class"] = prediction
# predictions["scores"] = scores
#
# print(predictions)
#
# predictions.sort_values('scores', inplace=True, ascending=True, axis=0)
#
# print(len(predictions))
# print(predictions)
# predictions.to_csv(set_direccion, header=header, index=None, sep=',', mode='w')

