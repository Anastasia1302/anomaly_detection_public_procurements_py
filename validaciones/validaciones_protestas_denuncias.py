import pandas as pd
import os
import json
import numpy as np
import re

# direccion = 'iforest_results/tender_written_training_6/'
# set_direccion = 'validaciones/tender_written_training_6.csv'


def validaciones_protestas_denuncias(predictions):
    resultados = pd.DataFrame()
    # result_estimators = []
    # result_samples = []
    # result_features = []
    result_anomalias_sin_protesta_sin_denuncias = []
    result_anomalias_muy_anomalos_sin_protesta_sin_denuncias = []
    result_anomalias_muy_normales_sin_protesta_sin_denuncias = []
    total_sin_protesta_sin_denuncias = []
    result_anomalias_protestas = []
    result_anomalias_muy_anomalos_protestas = []
    result_anomalias_muy_normales_protestas = []
    total_protestas = []
    result_anomalias_denuncias = []
    result_anomalias_muy_anomalos_denuncias = []
    result_anomalias_muy_normales_denuncias = []
    total_denuncias = []

    # for filename in sorted(os.listdir(direccion)):
    # filename = '../../iforest_results/contratos_written_training/iforest_contratos_written_training_estimators_5_samples_50_features_1.csv'
    # features = re.search('features_(.*).csv', filename)
    # if int(features.group(1)) == 45:
    # print(filename)
    # textfile = direccion + filename
    # print(textfile)
    # predictions = pd.read_csv(textfile, sep=sep)

    protestas_file = "../train_models/respuesta_dncp/53079-Protestascsv-Protestas.csv"
    protestas = pd.read_csv(protestas_file, sep=";")

    denuncias_file = "../train_models/respuesta_dncp/36500-Denunciascsv-Denuncias.csv"
    denuncias = pd.read_csv(denuncias_file, sep=";")

    anomaly_score_protestas = pd.DataFrame()
    anomaly_score_denuncias = pd.DataFrame()
    sin_protesta_sin_denuncias = pd.DataFrame()
    protestas_ids = []
    protestas_contract_score = []
    denuncias_ids = []
    denuncias_contract_score = []
    sin_protesta_sin_denuncias_ids = []
    sin_protesta_sin_denuncias_contract_score = []

    for (index, value) in predictions["id"].iteritems():
        # if value in new_contract_data["id"].values: #for new data
        pos_protestas = np.where(protestas["id"] == value)
        pos_denuncias = np.where(denuncias["id"] == value)
        # if len(pos_protestas[0]) == 0 and len(pos_denuncias[0]) == 0:
        sin_protesta_sin_denuncias_ids.append(predictions["id"][index])
        sin_protesta_sin_denuncias_contract_score.append(predictions["scores"][index])
        if len(pos_protestas[0] > 0):
            for i in pos_protestas[0]:
                protestas_ids.append(predictions["id"][index])
                protestas_contract_score.append(predictions["scores"][index])
        if len(pos_denuncias[0] > 0):
            for i in pos_denuncias[0]:
                denuncias_ids.append(predictions["id"][index])
                denuncias_contract_score.append(predictions["scores"][index])

    # estimator = re.search('estimators_(.*)_samples', filename)
    # result_estimators.append(int(estimator.group(1)))
    # samples = re.search('samples_(.*)_features', filename)
    # result_samples.append(int(samples.group(1)))
    # features = re.search('features_(.*).csv', filename)
    # result_features.append(int(features.group(1)))

    anomaly_score_protestas["id"] = protestas_ids
    anomaly_score_protestas["scores"] = protestas_contract_score

    total_protestas.append(len(anomaly_score_protestas["id"]))
    result_anomalias_protestas.append(
        len(anomaly_score_protestas[anomaly_score_protestas["scores"] < 0]))
    result_anomalias_muy_anomalos_protestas.append(
        len(anomaly_score_protestas[anomaly_score_protestas["scores"] < -0.1]))
    result_anomalias_muy_normales_protestas.append(
        len(anomaly_score_protestas[anomaly_score_protestas["scores"] > 0.1]))

    anomaly_score_denuncias["id"] = denuncias_ids
    anomaly_score_denuncias["scores"] = denuncias_contract_score

    total_denuncias.append(len(anomaly_score_denuncias["id"]))
    result_anomalias_denuncias.append(
        len(anomaly_score_denuncias[anomaly_score_denuncias["scores"] < 0]))
    result_anomalias_muy_anomalos_denuncias.append(
        len(anomaly_score_denuncias[anomaly_score_denuncias["scores"] < -0.1]))
    result_anomalias_muy_normales_denuncias.append(
        len(anomaly_score_denuncias[anomaly_score_denuncias["scores"] > 0.1]))

    sin_protesta_sin_denuncias["id"] = sin_protesta_sin_denuncias_ids
    sin_protesta_sin_denuncias["scores"] = sin_protesta_sin_denuncias_contract_score

    total_sin_protesta_sin_denuncias.append(len(sin_protesta_sin_denuncias["id"]))
    result_anomalias_sin_protesta_sin_denuncias.append(
        len(sin_protesta_sin_denuncias[sin_protesta_sin_denuncias["scores"] < 0]))
    result_anomalias_muy_anomalos_sin_protesta_sin_denuncias.append(
        len(sin_protesta_sin_denuncias[sin_protesta_sin_denuncias["scores"] < -0.1]))
    result_anomalias_muy_normales_sin_protesta_sin_denuncias.append(
        len(sin_protesta_sin_denuncias[sin_protesta_sin_denuncias["scores"] > 0.1]))

    # print(result_anomalias_sin_protesta_sin_denuncias)
    # print(result_anomalias_muy_anomalos_sin_protesta_sin_denuncias)
    # print(result_anomalias_muy_normales_sin_protesta_sin_denuncias)
    # print(total_sin_protesta_sin_denuncias)

    # resultados["estimators"] = result_estimators
    # resultados["samples"] = result_samples
    # resultados["features"] = result_features
    resultados["anomalias"] = result_anomalias_sin_protesta_sin_denuncias
    resultados["muy_anormales"] = result_anomalias_muy_anomalos_sin_protesta_sin_denuncias
    resultados["muy_normales"] = result_anomalias_muy_normales_sin_protesta_sin_denuncias
    resultados["total"] = total_sin_protesta_sin_denuncias
    resultados["anomalias_protestas"] = result_anomalias_protestas
    resultados["muy_anormales_protestas"] = result_anomalias_muy_anomalos_protestas
    resultados["muy_normales_protestas"] = result_anomalias_muy_normales_protestas
    resultados["total_protestas"] = total_protestas
    resultados["anomalias_denuncias"] = result_anomalias_denuncias
    resultados["muy_anormales_denuncias"] = result_anomalias_muy_anomalos_denuncias
    resultados["muy_normales_denuncias"] = result_anomalias_muy_normales_denuncias
    resultados["total_denuncias"] = total_denuncias
    return resultados

# resultados.sort_values(by=['estimators', 'samples', 'features'], inplace=True)
#
# resultados.to_csv(set_direccion, header=['estimators', 'samples', 'features',
#                                          'anomalias',
#                                          'muy_anormales',
#                                          'muy_normales',
#                                          'total',
#                                          'anomalias_protestas',
#                                          'muy_anormales_protestas',
#                                          'muy_normales_protestas',
#                                          'total_protestas',
#                                          'anomalias_denuncias',
#                                          'muy_anormales_denuncias',
#                                          'muy_normales_denuncias',
#                                          'total_denuncias'
#                                          ], index=None, sep=',', mode='w')
