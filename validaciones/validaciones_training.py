import pandas as pd
import os
import json
import numpy as np
import re

direccion = 'iforest_results/tender_written_validaciones_1/'
set_direccion = 'validaciones/tender_written_validaciones_1_2019.csv'

resultados = pd.DataFrame()
result_estimators = []
result_samples = []
result_features = []
result_anomalias = []
result_anomalias_muy_anomalos = []
result_anomalias_muy_normales = []
total = []

for filename in sorted(os.listdir(direccion)):
    print(filename)
    textfile = direccion + filename
    predictions = pd.read_csv(textfile)


    estimator = re.search('estimators_(.*)_samples', filename)
    result_estimators.append(int(estimator.group(1)))
    samples = re.search('samples_(.*)_features', filename)
    result_samples.append(int(samples.group(1)))
    features = re.search('features_(.*).csv', filename)
    result_features.append(int(features.group(1)))

    total.append(len(predictions["OCDS ID"]))
    result_anomalias.append(len(predictions[predictions["Scores"] < 0]))
    result_anomalias_muy_anomalos.append(len(predictions[predictions["Scores"] < -0.1]))
    result_anomalias_muy_normales.append(len(predictions[predictions["Scores"] > 0.1]))


resultados["estimators"] = result_estimators
resultados["samples"] = result_samples
resultados["features"] = result_features
resultados["anomalias"] = result_anomalias
resultados["muy_anormales"] = result_anomalias_muy_anomalos
resultados["muy_normales"] = result_anomalias_muy_normales
resultados["total"] = total

resultados.sort_values(by=['estimators', 'samples', 'features'], inplace=True)

print(resultados)

resultados.to_csv(set_direccion, header=['estimators', 'samples', 'features',
                                         'anomalias',
                                         'muy_anormales',
                                         'muy_normales',
                                         'total'
                                         ], index=None, sep=',', mode='w')
